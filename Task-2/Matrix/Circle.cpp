#include <iostream>
#include <iomanip>
#include <string>
#include <math.h>

using namespace std;

int main(int argc, char *argv[])
{

    (void)argc;
    (void)argv;
    int iSize;
    cout<<"ВВедите размерность матрицы [1-50]:";
    cin>>iSize;
    if(iSize<1||50<iSize)
    {
        cout<<"Ошибка ввода данных"<<endl;
        return -1;
    }
    const int mSize = (iSize-1)*2+1;
    const int x = mSize/2, y = mSize/2;
    const int cWidth = (int)log10((int)sqrt(x*x+y*y)+1)+2;
    for (int row = 0; row<mSize; ++row)
    {
        cout<<endl;
        for(int column=0; column<mSize; ++column)
        {
            cout<<setw(cWidth)<<(int)sqrt((column-x)*(column-x)+(row-y)*(row-y))+1;
        }
    }
    cout<<endl;
    return 0;
}
