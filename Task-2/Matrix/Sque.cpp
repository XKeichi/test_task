#include <iostream>
#include <iomanip>
#include <string>
#include <math.h>
using namespace std;
int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;
    int iSize;
    cout<<"ВВедите размерность матрицы [1-50]:";
    cin>>iSize;
    if(iSize<1||50<iSize)
    {
        cout<<"Ошибка ввода данных"<<endl;
        return -1;
    }
    const int mSize = (iSize-1)*2+1;
    const int cWidth = (int)log10(iSize)+2;
    for (int row = 0; row<iSize; ++row)
    {
        cout<<endl;
        for(int column=0;column<row%iSize; ++column)
            cout<<setw(cWidth)<<iSize-column;
        for(int column=0;column<mSize-(row%iSize)*2; ++column)
           cout<<setw(cWidth)<<iSize-row;
        for(int column=0;column<row%iSize; ++column)
           cout<<setw(cWidth)<<iSize-(row%iSize-column)+1;

    }
    for (int row = 1; row<iSize; ++row)
    {
        cout<<endl;
        for(int column=0;column<iSize-row%iSize; ++column)
            cout<<setw(cWidth)<<iSize-column;
        for(int column=0;column<mSize-((iSize-row)%iSize)*2; ++column)
           cout<<setw(cWidth)<<row+1;
        for(int column=0;column<iSize-row%iSize; ++column)
           cout<<setw(cWidth)<<row+column+1;


    }
    cout<<endl;
    return 0;
}
