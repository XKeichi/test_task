#include <iostream>
#include <string>
#include <iosfwd>
#include <iomanip>
#include <algorithm>
#include <bitset>
using namespace std;
int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;
    string Message;
    cout<<"Ввод сообщения:"<<endl;
    getline(cin,Message);
    cout<<"Таблица сообщения:"<<endl;
    for_each (Message.begin(), Message.end(),[](char c) {
        cout<<c<<":"<<std::hex<<(unsigned short)c<<"->"<<bitset<7>(c)<<endl;
    });
    string Coding_Message_Value;
    for (char iByte: Message)
    {
        unsigned short sBit = (uint16_t)-1;
        for(int iBit=0;iBit<7;++iBit)
        {
            unsigned short cBit  = (iByte>>(6-iBit))&0x1;
            if(cBit!=sBit)
            {
                Coding_Message_Value+=" ";
                Coding_Message_Value+=((cBit==1)?"0":"00");
                Coding_Message_Value+=" ";
                sBit = cBit;
            }
            Coding_Message_Value+="0";
        }
    }
    cout<<"Закодированное сообщение:"<<endl;
    cout<<Coding_Message_Value<<endl;
    return 1;
}
